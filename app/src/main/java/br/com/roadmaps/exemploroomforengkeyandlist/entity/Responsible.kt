package br.com.roadmaps.exemploroomforengkeyandlist.entity

import androidx.room.*
import br.com.roadmaps.exemploroomforengkeyandlist.helper.CardConverte
import br.com.roadmaps.exemploroomforengkeyandlist.helper.DependentsConverter


@Entity(tableName = "responsible")
class Responsible(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "idResponsible")
    var idResponsible: Long,

    @TypeConverters(DependentsConverter::class)
    var listDependents :List<Dependents>,

    @TypeConverters(CardConverte::class)
    val card: Card,

    @ColumnInfo(name = "nameResponsible")
    var nameResponsible: String,

    @Embedded
    var address: Address){


    override fun toString(): String {
        var value = "ID: $idResponsible\n" +
                "NOME: $nameResponsible\n\n" +
                "------------------- CARD -------------------\n" +
                "ID CARD: ${card.idCard}\n" +
                "NOME CARD: ${card.nameCard}\n" +
                "NUMERO CARD: ${card.numberCard}\n\n" +
                "------------------- ENDEREÇO -------------------\n" +
                "ID ENDEREÇO: ${address.idAddress}\n" +
                "RUA: ${address.street}\n" +
                "NUMERO: ${address.number}\n" +
                "CIDADE: ${address.city}\n" +
                "ESTADO: ${address.state}\n\n"

        if (listDependents != null && listDependents.isNotEmpty()){
            value += "-------------------DEPENDENTES -------------------\n"
            listDependents.map {
                value += "NOME: ${it.nameDepen}\n IDADE: ${it.age}\n\n"
            }
        }

        return value

    }
}


