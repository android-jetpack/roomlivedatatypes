package br.com.roadmaps.exemploroomforengkeyandlist.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import br.com.roadmaps.exemploroomforengkeyandlist.entity.Responsible

@Dao
interface ResponsibleDao {

    @Insert
    fun insert(responsible: Responsible?)


    @Update
    fun update(responsible: Responsible?)


    @Delete
    fun delete(responsible: Responsible?)


    @Query("SELECT * FROM responsible")
    fun getAllResponsible(): LiveData<List<Responsible>>

    @Query("SELECT * FROM responsible")
    fun getResponsible(): LiveData<Responsible>


    @Query("DELETE FROM responsible")
    fun deleteTableResponsible()
}