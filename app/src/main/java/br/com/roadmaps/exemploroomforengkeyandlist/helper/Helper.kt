package br.com.roadmaps.exemploroomforengkeyandlist.helper

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import br.com.roadmaps.exemploroomforengkeyandlist.dao.ResponsibleDao
import br.com.roadmaps.exemploroomforengkeyandlist.entity.Address
import br.com.roadmaps.exemploroomforengkeyandlist.entity.Card
import br.com.roadmaps.exemploroomforengkeyandlist.entity.Responsible


@Database(entities = [Responsible::class, Card::class, Address::class], version = 1)
@TypeConverters(CardConverte::class, DependentsConverter::class)
abstract class Helper: RoomDatabase() {


    abstract fun responsibleDAO(): ResponsibleDao


    companion object {

        private var INSTANCE: Helper? = null

        fun getInstance(context: Context): Helper? {
            if (INSTANCE == null) {
                synchronized(Helper::class) {
                    INSTANCE = Room.databaseBuilder(context, Helper::class.java,
                        "database_foreng_convert.db")
                        .build()
                }
            }
            return INSTANCE
        }
    }
}