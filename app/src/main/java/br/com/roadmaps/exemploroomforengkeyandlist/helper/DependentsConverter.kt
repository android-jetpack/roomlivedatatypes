package br.com.roadmaps.exemploroomforengkeyandlist.helper

import androidx.room.TypeConverter
import br.com.roadmaps.exemploroomforengkeyandlist.entity.Dependents
import com.google.gson.Gson


class DependentsConverter {

    @TypeConverter
    fun listToJson(value: List<Dependents>?) = Gson().toJson(value)

    @TypeConverter
    fun jsonToList(value: String) = Gson().fromJson(value, Array<Dependents>::class.java).toList()
}