package br.com.roadmaps.exemploroomforengkeyandlist

import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import br.com.roadmaps.exemploroomforengkeyandlist.entity.Address
import br.com.roadmaps.exemploroomforengkeyandlist.entity.Card
import br.com.roadmaps.exemploroomforengkeyandlist.entity.Dependents
import br.com.roadmaps.exemploroomforengkeyandlist.entity.Responsible
import br.com.roadmaps.exemploroomforengkeyandlist.helper.Helper
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {

    var helper: Helper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        helper = Helper.getInstance(this)

    }



    fun btInsert(v: View){
        doAsync{
            val address = Address(0, "Rua paraná", "120", "Camaragibe", "Pernambuco")
            val card = Card(0,"Nubank","123456789")
            val listDepen = ArrayList<Dependents>()
            listDepen.add(Dependents("Maria", 18))
            listDepen.add(Dependents("joao", 20))
            helper?.responsibleDAO()?.insert(Responsible(0, listDepen, card, "Rodrigo", address))
        }.execute()
    }


    fun btList(v: View){
        val responsibleLiveData = helper?.responsibleDAO()?.getAllResponsible()
        responsibleLiveData?.observe(this, androidx.lifecycle.Observer<List<Responsible>> {
            it.map { Log.i("VALUE_RESPOS",it.toString()) }
        })
    }


    fun btDelete(v: View){
        val responsibleLiveData = helper?.responsibleDAO()?.getResponsible()
        responsibleLiveData?.observe(this, androidx.lifecycle.Observer<Responsible> {
            doAsync {
                helper?.responsibleDAO()?.delete(it)
            }.execute()
        })
    }
}

