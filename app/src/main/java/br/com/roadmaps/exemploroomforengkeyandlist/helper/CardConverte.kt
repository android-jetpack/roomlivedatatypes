package br.com.roadmaps.exemploroomforengkeyandlist.helper

import androidx.room.TypeConverter
import br.com.roadmaps.exemploroomforengkeyandlist.entity.Card
import com.google.gson.Gson


class CardConverte {

    @TypeConverter
    fun cardToString(card: Card): String = Gson().toJson(card)

    @TypeConverter
    fun stringToCard(string: String): Card = Gson().fromJson(string, Card::class.java)

}