package br.com.roadmaps.exemploroomforengkeyandlist.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "address")
class Address(

    @PrimaryKey(autoGenerate = true)
    var idAddress: Long,
    var street: String,
    var number: String,
    var city: String,
    var state: String) {}