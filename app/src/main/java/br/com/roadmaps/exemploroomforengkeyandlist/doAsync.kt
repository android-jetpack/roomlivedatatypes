package br.com.roadmaps.exemploroomforengkeyandlist

import android.os.AsyncTask
import androidx.annotation.WorkerThread
import java.lang.NullPointerException


class doAsync(val handler: () -> Unit) : AsyncTask<Void, Void, Void>() {

    @WorkerThread
    override fun doInBackground(vararg params: Void?): Void? {
        try{
            handler()
        }catch (e: NullPointerException){}
        return null
    }
}