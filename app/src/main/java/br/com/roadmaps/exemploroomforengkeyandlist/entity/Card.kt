package br.com.roadmaps.exemploroomforengkeyandlist.entity

import androidx.room.*


@Entity(tableName = "card")
class Card(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "idCard")
    var idCard: Long,


    @ColumnInfo(name = "nameCard")
    var nameCard: String,


    @ColumnInfo(name = "numberCard")
    var numberCard: String){
}